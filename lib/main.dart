
import 'package:flutter/material.dart';
import 'package:trabajofinal/pages/inicio.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context){
    return const MaterialApp(
      title: "Material APP",
      debugShowCheckedModeBanner: false,
      home: Porta(),
    );
  }
}