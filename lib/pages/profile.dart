import 'package:flutter/material.dart';
import 'crashpage.dart';

class Perfil extends StatelessWidget {
  const Perfil({Key? key}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    final items = List.generate(3, (i) => Column(
      children: [
        Text("Dato $i del perfil",style: const TextStyle(fontSize: 30,color: Colors.black,),),
        const Divider(),
      ],
    ),
    );
    return Scaffold(
      body: Stack(
        children: [
          const SizedBox(
              width: 500,
              height: double.infinity,
              child: Image(
                  image: AssetImage('assets/img/Oficina.PNG'),
                  fit: BoxFit.cover)),
          Column(
            children: [
              Container(
                height: 25,
              ),
              Row(
                children: [
                  GestureDetector(
                    child: const Image(
                        image: AssetImage("assets/iconos/Flecha.PNG")),
                    onTap: () => Navigator.of(context).pop(),
                  ),
                  const Spacer(),
                  GestureDetector(
                      child: const Image(
                          image: AssetImage("assets/iconos/Menu.PNG")),
                      onTap: () {
                        var route = MaterialPageRoute(
                          builder: (context) => const Crash(),
                        );
                        Navigator.of(context).push(route);
                      }),
                  GestureDetector(
                      child: const Image(
                          image: AssetImage("assets/iconos/Opciones.PNG")),
                      onTap: () {
                        var route = MaterialPageRoute(
                          builder: (context) => const Crash(),
                        );
                        Navigator.of(context).push(route);
                      }),
                ],
              ),
              const Spacer(),
              SizedBox(
                width: 400,
                child: Row(
                  children: [
                    Container(
                        width: 120,
                        height: 120,
                        decoration: BoxDecoration(
                          image: const DecorationImage(
                              image: AssetImage("assets/img/perfil.PNG")),
                          color: Colors.white60.withOpacity(1),
                          borderRadius:
                              const BorderRadius.all(Radius.circular(90)),
                        )),
                    const Spacer(flex: 1),
                    const Text(
                      "Dwight K. Schrute",
                      style: TextStyle(
                          fontSize: 24,
                          color: Colors.black,
                          fontWeight: FontWeight.bold),
                    ),
                    const Spacer(flex: 5),
                  ],
                ),
              ),
              Container(
                  width: 400,
                  height: 300,
                  padding: const EdgeInsets.all(20),
                  decoration: BoxDecoration(
                      color: Colors.white60.withOpacity(1),
                      borderRadius:
                          const BorderRadius.all(Radius.circular(10))),
                  child: ListView(children: items)),
              const Spacer(),

            ],
          ),
        ],
      ),
    );
  }
}
