import 'package:flutter/material.dart';

import 'menu.dart';
class Cargando2 extends StatelessWidget{
  const Cargando2({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context){
    espera(context);
    return Stack(
      children: const [
        SizedBox(width: 10000, height: double.infinity, child: Image(image: AssetImage('assets/img/Oficina.PNG'), fit: BoxFit.fitHeight,),),
        Center(
          child:
          SizedBox(
            height: 180,
            width: 180,
            child: CircularProgressIndicator(
                strokeWidth: 10,
                backgroundColor: Colors.amber,
                color: Colors.orange
            ),
          ),
        )
      ],
    );
  }
}
Future<void> espera(context)async {
  var route =MaterialPageRoute(builder: (context) => const Menu());
  return Future.delayed(const Duration(seconds: 1),() => Navigator.of(context).push(route));
}