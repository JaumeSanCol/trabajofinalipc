import 'package:flutter/material.dart';

import 'login.dart';

class Porta extends StatelessWidget {
  const Porta({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: Stack(
            children: [
              const SizedBox(width: 1000, height: 1000, child: Image(image: AssetImage('assets/img/portada.jpg'), fit: BoxFit.none,)
              ),
              Center(
                  child: GestureDetector(
                    child: const SizedBox(height:350, width: 350,child:Center(child: SizedBox(width: 100, height: 100, child: Image(image: AssetImage('assets/iconos/tap.png'))))),
                    onTap: () {var route = MaterialPageRoute(builder: (context) => const LoginPage(),);Navigator.of(context).push(route);},
                  )
              )
            ]
        )
    );
  }
}
