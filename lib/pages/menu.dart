import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:trabajofinal/pages/crashpage.dart';
import 'package:trabajofinal/pages/login.dart';
import 'package:trabajofinal/pages/products.dart';
import 'package:trabajofinal/pages/profile.dart';
import 'package:trabajofinal/pages/shop_list.dart';
import 'package:tuple/tuple.dart';

import '../model/icon_model.dart';
import 'cargando.dart';

class Menu extends StatelessWidget {
  const Menu({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final items = Papel.initial.map((ppl) {
      return Column(
        children: [
          Container(
            width: 400,
            height: 75,
            decoration: BoxDecoration(
                color: Colors.brown.withOpacity(0.8),
                borderRadius: const BorderRadius.all(Radius.circular(10))),
            child: ListTile(
                trailing: SizedBox(
                  height: 100,
                  width: 125,
                  child: Center(
                    child: Column(
                      children: [
                        const Spacer(
                          flex: 10,
                        ),
                        Text("Price ${ppl.price} €",
                            style: GoogleFonts.getFont(
                              "Varela Round",
                              fontSize: 15,
                              textStyle: const TextStyle(
                                  color: Colors.black,
                                  fontWeight: FontWeight.bold),
                            )),
                        const Spacer(),
                        SizedBox(
                          height: 30,
                          width: 83,
                          child: GestureDetector(
                              child: Stack(
                                children: const [
                                  Image(
                                      image: AssetImage(
                                          "assets/iconos/Boton.PNG")),
                                  Center(
                                      child: Text(
                                        "Añadir",
                                        style: TextStyle(
                                            color: Colors.black54,
                                            fontWeight: FontWeight.bold),
                                      )),
                                ],
                              ),
                              onTap: () {var route = MaterialPageRoute(
                                builder: (context) => const Cargando(),
                              );
                              Navigator.of(context).push(route);

                              }),
                        ),
                      ],
                    ),
                  ),
                ),
                title: Text(ppl.name,
                    style: GoogleFonts.getFont(
                      "Varela Round",
                      fontSize: 16,
                      textStyle: const TextStyle(
                          color: Colors.black, fontWeight: FontWeight.bold),
                    )),
                leading: Hero(
                  tag: ppl.name,
                  child: Image.asset("${ppl.iconPath}"),
                ),
                onTap: () {
                  var route = MaterialPageRoute(
                    builder: (context) => Info(data: Tuple4(ppl.name, ppl.price, "${ppl.iconPath}",ppl.descri)),
                  );
                  Navigator.of(context).push(route);
                }),
          ),
        ],
      );
    }).toList();

    return Scaffold(
        body: Stack(children: [
          const SizedBox(
            width: 1000,
            height: double.infinity,
            child: Image(
              image: AssetImage('assets/img/Fondo1.PNG'),
              fit: BoxFit.cover,
            ),
          ),
          SizedBox(
            width: 500,
            height: double.infinity,
            child: Column(
              children: [
                Container(
                  height: 25,
                ),
                Row(
                  children: [
                    GestureDetector(
                      child: const Image(
                          image: AssetImage("assets/iconos/Flecha.PNG")),
                      onTap: () {
                        var route = MaterialPageRoute(
                          builder: (context) => const LoginPage(),
                        );
                        Navigator.of(context).push(route);
                      },
                    ),
                    const Spacer(),
                    GestureDetector(
                        child: const Image(
                            image: AssetImage("assets/iconos/Menu.PNG")),
                        onTap: () {
                          var route = MaterialPageRoute(
                            builder: (context) => const Crash(),
                          );
                          Navigator.of(context).push(route);
                        }),
                    GestureDetector(
                        child: const Image(
                            image: AssetImage("assets/iconos/Opciones.PNG")),
                        onTap: () {
                          var route = MaterialPageRoute(
                            builder: (context) => const Crash(),
                          );
                          Navigator.of(context).push(route);
                        }),
                  ],
                ),
                SizedBox(
                    height: 480,
                    width: 400,
                    child: ListView.separated(
                        itemBuilder: (context, i) => items[i],
                        separatorBuilder: (context, index) {
                          return const Divider();
                        },
                        itemCount: items.length)),
                Row(
                  children: [
                    const Spacer(flex: 5,),
                    SizedBox(height: 70,width: 195,
                      child: GestureDetector(
                          child: Stack(
                            children: const [
                              Image(image: AssetImage("assets/iconos/Boton.PNG")),
                              Center(
                                child: Text("Mi Cesta",

                                  style: TextStyle(fontSize: 28,

                                      color: Colors.black54, fontWeight: FontWeight.bold),

                                ),
                              )
                            ],
                          ),
                          onTap: () {
                            var route = MaterialPageRoute(
                              builder: (context) => const Shop(),
                            );
                            Navigator.of(context).push(route);
                          }),
                    ),
                    const Spacer(flex:1),
                    GestureDetector(
                        child: const Image(
                            image: AssetImage("assets/iconos/Menu.PNG")),
                        onTap: () {
                          var route = MaterialPageRoute(
                            builder: (context) => const Perfil(),
                          );
                          Navigator.of(context).push(route);
                        }),
                    Container(
                      width: 5,
                    )
                  ],
                ),
              ],
            ),
          )
        ]));
  }
}
