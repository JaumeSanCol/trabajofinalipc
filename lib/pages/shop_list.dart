import 'package:flutter/material.dart';
import 'package:trabajofinal/pages/cargando.dart';
import 'package:trabajofinal/pages/cargando2.dart';
import 'package:trabajofinal/pages/login.dart';
import 'crashpage.dart';

class Shop extends StatelessWidget {

  const Shop({Key? key}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Stack(
        children: [
          const SizedBox(
              width: 500,
              height: double.infinity,
              child: Image(
                  image: AssetImage('assets/img/Oficina.PNG'),
                  fit: BoxFit.cover)),
          Column(
            children: [
              Container(
                height: 25,
              ),
              Row(
                children: [
                  GestureDetector(
                    child: const Image(
                        image: AssetImage("assets/iconos/Flecha.PNG")),
                    onTap: () => Navigator.of(context).pop(),
                  ),
                  const Spacer(),
                  GestureDetector(
                      child: const Image(
                          image: AssetImage("assets/iconos/Menu.PNG")),
                      onTap: () {
                        var route = MaterialPageRoute(
                          builder: (context) => const Crash(),
                        );
                        Navigator.of(context).push(route);
                      }),
                  GestureDetector(
                      child: const Image(
                          image: AssetImage("assets/iconos/Opciones.PNG")),
                      onTap: () {
                        var route = MaterialPageRoute(
                          builder: (context) => const Crash(),
                        );
                        Navigator.of(context).push(route);
                      }),
                ],
              ),
              SizedBox(
                width: 400,
                child: Row(
                  children: const [

                    SizedBox(
                      width: 250,
                      height: 200,
                      child: Image(image: AssetImage("assets/img/Michael1.PNG")),
                    ),
                    Spacer(),
                  ],
                ),
              ),
              Container(
                width: 400,
                height: 300,
                padding: const EdgeInsets.all(20),
                decoration: BoxDecoration(
                    color: Colors.white60.withOpacity(1),
                    borderRadius: const BorderRadius.all(Radius.circular(10))),
                child: Column(
                  children: const [
                    Spacer(),
                    Text("La cesta esta vacia",style: TextStyle(fontSize: 25, color: Colors.grey),),
                    Spacer(flex: 2),
                  ],
                ),
              ),
              const Spacer(),
              Row(
                children: [
                  const SizedBox(width: 113,
                  ),
                  SizedBox(
                    height: 60,
                    width: 185,
                    child: GestureDetector(
                        child: Stack(
                          children: const [
                            Image(image: AssetImage("assets/iconos/BotonPulsado.PNG")),
                            Center(
                              child: Text(
                                "Pagar",
                                style: TextStyle(
                                    fontSize: 26,
                                    color: Colors.black54,
                                    fontWeight: FontWeight.bold),
                              ),
                            )
                          ],
                        ),
                        onTap: () {
                         showDialog(
                             context: context,
                             builder: (context) => AlertDialog(
                               title: const Text("¿Finalizar Compra?"),
                               content:const Text("¿Está seguro de querer finalizar la compra?"),
                               actions:[
                                 GestureDetector(
                                   child: const SizedBox(height:50,width: 50,child:Image(image:AssetImage("assets/iconos/Yes.PNG"))),
                                   onTap:(){ var route = MaterialPageRoute(builder: (context) => const Cargando2(),);Navigator.of(context).push(route);},),
                                 const Spacer(),
                                 GestureDetector(
                                     child: const SizedBox(height:50,width: 50, child:Image(image:AssetImage("assets/iconos/No.PNG"))),
                                     onTap:() => Navigator.of(context).pop()),
                               ]
                             ),
                         );
                        }),
                  ), const Spacer(),
                ],
              ),const Spacer(),

            ],
          ),

        ],
      ),
    );
  }
}
