import 'package:flutter/material.dart';
import 'menu.dart';

class LoginPage extends StatelessWidget {
  const LoginPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: Stack(
      children: [
        const SizedBox(width: 10000, height: double.infinity, child: Image(image: AssetImage('assets/img/Fondo1.PNG'), fit: BoxFit.cover,),),
        Center(
          child: Column(
            children: [
              const Image(image: AssetImage("assets/img/Michael1.PNG")),
              Container(width: 350, height: 230, decoration: BoxDecoration(color: Colors.white,borderRadius: BorderRadius.circular(15),),
                  child: Center(
                    child: Column(
                      children: [
                        const Spacer(flex: 5,),
                        Row(
                          children: [
                            Container(width: 14),
                            const Icon(Icons.supervised_user_circle_rounded, color: Colors.orange, size: 25.0,),
                            const SizedBox(width: 8),
                            const Text("Username"),
                            const Spacer(),
                          ],
                        ),
                        const SizedBox(width: 300, height: 30, child: Center(child: TextField(obscureText: false, textAlign: TextAlign.start,),
                          ),
                        ),
                        const Spacer(),
                        Row(
                          children: [
                            Container(width: 14),
                            const Icon(Icons.vpn_key, color: Colors.orange, size: 25.0,),
                            const SizedBox(width: 8),
                            const Text("Password"),
                            const Spacer(),
                          ],
                        ),
                        const SizedBox(width: 300, height: 30, child: Center(child: TextField(obscureText: false, textAlign: TextAlign.start,),),),
                        const Spacer(),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            GestureDetector(child: const SizedBox(width: 150, height: 80, child: Center(child: Image(image: AssetImage("assets/iconos/Login.PNG"))),),
                              onTap: () {var route = MaterialPageRoute(builder: (context) =>  const Menu(),);Navigator.of(context).push(route);},
                            ),
                          ],
                        ),
                        const Spacer(flex: 1,),
                      ],
                    ),
                  )),
            ],
          ),
        ),
      ],
    ));
  }
}
