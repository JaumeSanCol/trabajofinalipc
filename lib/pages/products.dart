import 'package:flutter/material.dart';
import 'package:trabajofinal/pages/cargando.dart';
import 'package:trabajofinal/pages/profile.dart';
import 'package:tuple/tuple.dart';

import 'crashpage.dart';

class Info extends StatelessWidget {
  Info({Key? key, required this.data}) : super(key: key);
  final GlobalKey<ScaffoldState> _key = GlobalKey();
  final Tuple4<String, double, String?, String> data;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _key,
      body: Stack(
        children: [
          const SizedBox(
              width: 500,
              height: double.infinity,
              child: Image(
                  image: AssetImage('assets/img/Oficina.PNG'),
                  fit: BoxFit.cover)),
          Column(
            children: [
              Container(
                height: 25,
              ),
              Row(
                children: [
                  GestureDetector(
                    child: const Image(
                        image: AssetImage("assets/iconos/Flecha.PNG")),
                    onTap: () => Navigator.of(context).pop(),
                  ),
                  const Spacer(),
                  GestureDetector(
                      child: const Image(
                          image: AssetImage("assets/iconos/Menu.PNG")),
                      onTap: () {
                        var route = MaterialPageRoute(
                          builder: (context) => const Crash(),
                        );
                        Navigator.of(context).push(route);
                      }),
                  GestureDetector(
                      child: const Image(
                          image: AssetImage("assets/iconos/Opciones.PNG")),
                      onTap: () {
                        var route = MaterialPageRoute(
                          builder: (context) => const Crash(),
                        );
                        Navigator.of(context).push(route);
                      }),
                ],
              ),
              SizedBox(
                width: 400,
                child: Row(
                  children: [
                    Container(
                      width: 140,
                      height: 140,
                      decoration: const BoxDecoration(
                          color: Colors.brown,
                          borderRadius: BorderRadius.all(Radius.circular(60))),
                      child: Center(
                        child: Hero(
                          tag: data.item1,
                          child: SizedBox(
                            width: 120,
                            height: 120,
                            child: Image.asset("${data.item3}"),
                          ),
                        ),
                      ),
                    ),
                    const Spacer(),
                    const SizedBox(
                      width: 250,
                      height: 200,
                      child: Image(
                          image: AssetImage("assets/img/Stanley.PNG")),
                    )
                  ],
                ),
              ),
              Container(
                width: 400,
                height: 200,
                padding: const EdgeInsets.all(20),
                decoration: BoxDecoration(
                    color: Colors.white60.withOpacity(1),
                    borderRadius: const BorderRadius.all(Radius.circular(10))),
                  child: Column(
                    children: [
                      const Spacer(),
                      Text(data.item1,
                        textAlign: TextAlign.left,
                        style: const TextStyle(
                          fontSize: 24,
                          color: Colors.black,
                          fontWeight: FontWeight.bold,),
                      ),

                      const Spacer(flex: 1),
                      Text(data.item4,
                        style: const TextStyle(
                        fontSize: 16,
                        color: Colors.black54,),
                      ),
                      const Spacer(flex: 2),
                    ],
                  ),


              ),
              const Spacer(),
              Row(
                children: [
                  const Spacer(
                    flex: 5,
                  ),
                  SizedBox(
                    height: 70,
                    width: 195,
                    child: GestureDetector(
                        child: Stack(
                          children: const [
                            Image(image: AssetImage("assets/iconos/Boton.PNG")),
                            Center(
                              child: Text(
                                "Añadir a la cesta",
                                style: TextStyle(
                                    fontSize: 19,
                                    color: Colors.black54,
                                    fontWeight: FontWeight.bold),
                              ),
                            )
                          ],
                        ),
                        onTap: () {
                          var route = MaterialPageRoute(
                            builder: (context) => const Cargando(),
                          );
                          Navigator.of(context).push(route);
                        }),
                  ),
                  const Spacer(flex: 1),
                  GestureDetector(
                      child: const Image(
                          image: AssetImage("assets/iconos/Menu.PNG")),
                      onTap: () {
                        var route = MaterialPageRoute(
                          builder: (context) => const Perfil(),
                        );
                        Navigator.of(context).push(route);
                      }),
                  Container(
                    width: 5,
                  )
                ],
              ),
              const Spacer(),
            ],
          ),
        ],
      ),
    );
  }
}
