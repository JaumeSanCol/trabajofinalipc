import 'package:flutter/material.dart';
class Cargando extends StatelessWidget{
  const Cargando({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context){
    espera(context);
    return Stack(
      children: const [
        SizedBox(width: 10000, height: double.infinity, child: Image(image: AssetImage('assets/img/Oficina.PNG'), fit: BoxFit.fitHeight,),),
        Center(
          child:
            SizedBox(
              height: 180,
              width: 180,
              child: CircularProgressIndicator(
                  strokeWidth: 10,
                  backgroundColor: Colors.amber,
                  color: Colors.orange
              ),
            ),
        )
      ],
    );
  }
}
Future<void> espera(context)async {
  return Future.delayed(const Duration(seconds: 1),() => Navigator.of(context).pop());
}