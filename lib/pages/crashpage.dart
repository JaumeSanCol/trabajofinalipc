import 'package:flutter/material.dart';


class Crash extends StatelessWidget {
  const Crash({Key? key}) : super(key: key);


  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Error'),
      ),
      body: Center(
        child: Column(
          children:  const [
            Image(image: AssetImage('assets/img/Dwight2.PNG')),
            Center(
              child: Text('Page under construction, please turn back..'),
            )
          ],
        ),
      )
    );
  }
}