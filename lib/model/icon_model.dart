

import 'package:json_annotation/json_annotation.dart';
@JsonSerializable()
class Papel {
  final String name, descri;
  final double price;
  final String? iconPath;


  Papel({
    required this.name,
    required this.price,
    required this.iconPath,
    required this.descri,
  });

/*
  factory Papel.fromJson(Map<String, dynamic> data)=>
      Papel(
        name: data['name'],
        price :  data['price'],
        iconPath:  data['iconPath'],
        descri:  data['descri'],
    );

  Map<String,dynamic> toJson(){
    return{
      "name":name,
      'price':price,
      'iconPath':iconPath,
      'descri':descri,
    };
  }


}
*/
  static final List<Papel> initial = [
    Papel(name: "100h A4",
        price: 3.49,
        iconPath: "assets/iconos/A4.png",
        descri: "100 hojas de papel de oficina blancos de tamaño A4 y 100mg"),
    Papel(name: "200h A4",
        price: 5.99,
        iconPath: "assets/iconos/A4.png",
        descri: "200 hojas de papel de oficina blancos de tamaño A4 y 100mg"),
    Papel(name: "Caja A4",
        price: 19.99,
        iconPath: "assets/iconos/caja.png",
        descri: "Una caja de 1000 hojas de papel de oficina blancos de tamaño A4 y 100mg"),
    Papel(name: "Palé A4",
        price: 79.99,
        iconPath: "assets/iconos/Pale.png",
        descri: "Un palé de 6000 hojas papel de oficina blancos de tamaño A4 y 100mg"),
    Papel(name: "100h A2",
        price: 3.99,
        iconPath: "assets/iconos/A2.png",
        descri: "100 hojas de papel de oficina blancos de tamaño A2 y 100mg"),
    Papel(name: "200h A2",
        price: 6.49,
        iconPath: "assets/iconos/A2.png",
        descri: "200 hojas de papel de oficina blancos de tamaño A2 y 100mg"),
    Papel(name: "Caja A2",
        price: 24.99,
        iconPath: "assets/iconos/caja.png",
        descri: "Una caja de 1000 hojas de papel de oficina blancos de tamaño A2 y 100mg"),
    Papel(name: "Palé A2",
        price: 84.99,
        iconPath: "assets/iconos/Pale.png",
        descri: "Un palé de 6000 hojas papel de oficina blancos de tamaño A2 y 100mg"),
    Papel(name: "100h A3",
        price: 2.99,
        iconPath: "assets/iconos/A3.png",
        descri: "100 hojas de papel de oficina blancos de tamaño A3 y 100mg"),
    Papel(name: "200h A3",
        price: 4.99,
        iconPath: "assets/iconos/A3.png",
        descri: "200 hojas de papel de oficina blancos de tamaño A3 y 100mg"),
    Papel(name: "Caja A3",
        price: 14.99,
        iconPath: "assets/iconos/caja.png",
        descri: "Una caja de 1000 hojas de papel de oficina blancos de tamaño A3 y 100mg"),
    Papel(name: "Palé A3",
        price: 74.49,
        iconPath: "assets/iconos/Pale.png",
        descri: "Un palé de 6000 hojas papel de oficina blancos de tamaño A3 y 100mg"),
  ];


}
